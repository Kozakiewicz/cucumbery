Feature: Is it Friday yet?
  Everybody wants to know when it's Friday

#  Scenario: Sunday isn't Friday
#    Given today is Sunday
#    When I ask whether it's Friday yet
#    Then I should be told "Nope"

  Scenario: Parse enum abc
    Given I'm parsing enum "abc"

  Scenario: Parse enum invalid
    Given I'm parsing enum ""

  Scenario Outline: Parsing data containers
    Given Lets parse an object here <id>
      | id  | a  | b  |
      | 123 | 11 | 22 |
      | 321 | 33 | 44 |
    Examples:
      | id    | user     | party |
      | 10001 | "blabla" | 2     |

  Scenario Outline: Testing lists of strings
    When Abecadlo z pieca spadlo <abe> <cad> <lo>
    Examples:
      | abe      | cad              | lo               |
      | "simple" | "listOfParams"   | "another simple" |
      | "simple" | "list,OF,params" | "another simple" |

  Scenario: Testing datatables
    When Blablabla
      | first     | second |
      | "simple"  | 0.0    |
      | "simple2" |        |

  Scenario: Test passing multiple parameters
    When Passing "first" then some other params
      | a | b |

  Scenario Outline: Testing long with whitespace
    When Long params <a> and <b>
    Examples:
      | a   | b    |
      | "1" | "2 " |
      | "3" | "4 " |

  Scenario: Passing big decimals
    When Passing "3060.0", "3060.0000"

  Scenario: Passing longs with preceeding zeros
    When Passing some values "some string" "00000000518", "000000000123"

  Scenario: Passing weird data
    When Passing some values from table as list of strings
      | a | b | c |
      | d | e | f |

  Scenario: Passing weird data 2
    When Null values or string values transformed to list of maps
      | a | b | c | d    | e    | d |
      | d | e | f | null | NULL |   |

  Scenario: Passing weird data 3
    When I will pass list o strings
      | a | b | c | d | e | f |
    When I will pass list o strings
      | a |
      | b |
      | c |
      | d |
      | e |
      | f |
    When I will pass list o strings
      | a | b | c | d | e | f |
      | a | b | c | d | e | f |
    When I will pass list o strings
      | a | a |
      | b | b |
      | c | c |
      | d | d |
      | e | e |
      | f | f |