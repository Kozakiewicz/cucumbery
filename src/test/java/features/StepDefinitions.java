package features;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class StepDefinitions {
    @Given("^today is Sunday$")
    public void today_is_Sunday() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("today is Sunday");
//        throw new PendingException();
    }

    @When("^I ask whether it's Friday yet$")
    public void i_ask_whether_it_s_Friday_yet() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("I ask whether it's Friday yet");
//        throw new PendingException();
    }

    @Then("^I should be told \"(.*?)\"$")
    public void i_should_be_told(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
//        throw new PendingException();
        System.out.println("I am told " + arg1);
    }

    @Given("^I'm parsing enum \"(.*?)\"$")
    public void i_m_parsing_enum(MyEnumo arg1) throws Throwable {
        System.out.println(arg1);
    }

    @Given("^Lets parse an object here (\\d+)$")
    public void lets_parse_an_object_here(int arg1, List<MyContainer> arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
//        throw new PendingException();
        System.out.println(arg2);

    }

    @When("^Abecadlo z pieca spadlo \"(.*?)\" \"(.*?)\" \"(.*?)\"$")
    public void abecadlo_z_pieca_spadlo(String arg1, List<String> arg2, String arg3) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(arg2);
    }

    @When("^Blablabla$")
    public void blablabla(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        System.out.println(arg1);

    }

    @When("^Passing \"(.*?)\" then some other params$")
    public void passing_then_some_other_params(String arg1, DataTable arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        System.out.println(arg1);
    }

    @When("^Long params \"(.*?)\" and \"(.*?)\"$")
    public void long_params_and(Long arg1, Long arg2) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(arg1);
        System.out.println(arg2);
    }

    @When("^Passing \"(.*?)\", \"(.*?)\"$")
    public void passing(BigDecimal arg0, BigDecimal arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(arg1.toString());
        System.out.println(arg0.toString());
    }

    @When("^Passing some values \"(.*?)\" \"(.*?)\", \"(.*?)\"$")
    public void passing_some_values(String arg1, Long arg2, Long arg3) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        System.out.println(arg1);
        System.out.println(arg2);
        System.out.println(arg3);
    }

    @When("^Passing some values from table as list of strings$")
    public void passing_some_values_from_table_as_list_of_strings(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        System.out.println(arg1);
        List<String> strings = arg1.asList(String.class);
        System.out.println(strings);
    }

    @When("^Null values or string values transformed to list of maps$")
    public void null_values_or_string_values_transformed_to_list_of_maps(List<Map<String, String>> arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        System.out.println(arg1);
    }

    @When("^I will pass list o strings$")
    public void i_will_pass_list_o_strings(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        System.out.println(arg1);
        System.out.println(arg1.asList(String.class));
    }
}
